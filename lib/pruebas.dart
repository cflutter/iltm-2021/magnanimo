// en el archivo pruebas.dart
// ya te ayudé con una parte, pero la puedes modificar

// crea una clase que se llame Animal con un método
// llamada descripción, en el cual regreses la cadena
// 'soy un animal'

// crea otra clase llamada Perro , que herede de animal
// pero que cuando se llame su método descripción  regrese
// la cadena 'soy un perro'

// crea otra clase llamada Peluche, que tenga un método descripción
// que regrese la cadena 'soy un peluche'

// crea otra clase llamada Snoopy que implemente Perro y Peluche,
// en su método descripción debe regresar 'soy un perro de peluche'

// Crea una lista llamada Lista1 y llénala de al menos 5 instancias diferentes.
// Lista1 solo debe de poder almacenar Perro,
// al menos uno de sus elements debe de ser un Snoopy ,

// Crea una lista llamada Lista2 y llénala de al menos 5 instancias diferentes
// Lista2 solo debe de admitir Peluche
// Al menos uno de los elementos debe de ser un Snoopy

// Crea una lista llamada Lista3 y llénala de al menos 5 instancias diferentes
// Lista3 debe de admitir Perro, Peluche, Animal
// al menos uno de sus elementos debe de ser un Perro
// al menos uno de sus elementos debe de ser un Animal
// al menos uno de sus elementos debe de ser un Peluche
// ninguno de sus elementos debe de ser un Snoopy

// en todos los casos en la cadena regresada no incluye los ' '
