import 'package:clases/pruebas.dart';
import 'package:test/test.dart';

void main() {
  test('clase Animal descripción correcta', () {
    Animal animal = Animal();
    expect(animal.descripcion(), 'soy un animal');
  });

  test('clase Perro es un animal', () {
    Perro perro = Perro();
    expect(perro is Animal, true);
  });
  test('clase Perro descripcion correcta', () {
    Perro perro = Perro();
    expect(perro.descripcion(), 'soy un perro');
  });

  test('Clase Peluche es un perro', () {
    Peluche peluche = Peluche();
    expect(peluche is Perro, false);
  });
  test('Clase Peluche no es un Animal', () {
    Peluche peluche = Peluche();
    expect(peluche is Animal, false);
  });
  test('Clase Peluche descripcion correcta', () {
    Peluche peluche = Peluche();
    expect(peluche.descripcion(), 'soy un peluche');
  });

  test('Clase Snoopy es Perro', () {
    Snoopy snoopy = Snoopy();
    expect(snoopy is Perro, true);
  });
  test('Clase Snoopy es Peluche', () {
    Snoopy snoopy = Snoopy();
    expect(snoopy is Peluche, true);
  });
  test('Clase Snoopy descripcion correcta', () {
    Snoopy snoopy = Snoopy();
    expect(snoopy.descripcion(), 'soy un perro de peluche');
  });

  test('Lista1 al menos 5 elementos', () {
    expect(Lista1.length, greaterThanOrEqualTo(5));
  });

  test('Lista1 tiene puros Perro', () {
    expect(Lista1.every((elemento) => elemento is Perro), true);
  });
  test('Lista1 tiene al menos un Snoopy', () {
    expect(Lista1.any((elemento) => elemento is Snoopy), true);
  });

  test('Lista2 tiene al menos 5 elementos', () {
    expect(Lista2.length, greaterThanOrEqualTo(5));
  });

  test('Lista2 deben de ser puros Peluche', () {
    expect(Lista2.every((elemento) => elemento is Peluche), true);
  });

  test('Lista2 debe de tener al menos un Snoopy', () {
    expect(Lista2.any((elemento) => elemento is Snoopy), true);
  });

  test('Lista3 debe de tener al menos 5 elementos', () {
    expect(Lista3.length, greaterThanOrEqualTo(5));
  });

  test('Lista3 debe de tener al menos un Perro', () {
    expect(Lista3.any((elemento) => elemento is Perro), true);
  });

  test('Lista3 debe de tener al menos un Animal', () {
    expect(Lista3.any((elemento) => elemento is Animal), true);
  });
  test('Lista3 debe de tener al menos un Peluche', () {
    expect(Lista3.any((elemento) => elemento is Peluche), true);
  });

    test('Lista3 no debe de tener ni un Snoopy', () {
    expect(Lista3.any((elemento) => elemento is Snoopy), false);
  });
}
